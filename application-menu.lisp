;;; -*- Mode: Lisp; Package: ECLIPSE-EXTENSIONS -*-
;;; $Id: menu-1.lisp,v 1.1 2004/02/17 15:43:07 ihatchondo Exp $
;;;
;;; ECLIPSE. The Common Lisp Window Manager.
;;; Copyright (C) 2004 Iban HATCHONDO
;;; contact : hatchond@yahoo.fr
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

;;; This file define a small application menu manipulation interface.
;;; Here comes examples that shows how to use this small interface:
;;; 
;;; (eclipse-ext:define-application-menu 
;;;   (("Pause xmms" :application "xmms" :args ("-u"))
;;;    ("mozilla" :application "mozilla")
;;;    ("TV" :submenu (("taztv" :application "taztv")
;;;                    ("tvtime" :application "tvtime")))))
;;;
;;; (eclipse-ext:add-application-menu-entry ("gv")
;;;   :index 2 
;;;   :application "gv")
;;;
;;; (eclipse-ext:add-application-menu-entry ("TV-beta" :in-sub-menu-named "TV")
;;;   :index nil
;;;   :submenu (("taztv" :application "taztv-beta")
;;;             ("tvtime" :application "tvtime-beta")))
;;;
;;; (eclipse-ext:remove-application-menu-entry "gv")

(in-package :eclipse-extensions)

(export '(define-application-menu
	  add-application-menu-entry
	  remove-application-menu-entry))

;;;; Private.

(defun remove-entry (name entries &key (count 1))
  (labels ((__remove__ (name entries)
	     (loop for e in entries
		   if (and (> count 0) (string= name (car e))) do (decf count)
		   else if (listp (cdr e))
		        when (let ((submenu (__remove__ name (cdr e))))
			       (when submenu (cons (car e) submenu)))
			collect it end
		        else collect e)))
    (unless count (setf count 1))
    (__remove__ name entries)))

(defun add-entry (entry in-submenu index entries)
  (cond ((and (not in-submenu) (or (not index) (>= index (length entries))))
	 (concatenate 'list entries (list entry)))
	((and (not in-submenu) index)
	 (loop for e in entries for i from 0
	       when (= i index) collect entry end
	       collect e))
	(t (loop for e in entries collect 
		 (cond
		   ((string= in-submenu (car e))
		    (cons (car e) (add-entry entry nil index (cdr e))))
		   ((listp (cdr e))
		    (cons (car e) (add-entry entry in-submenu index (cdr e))))
		   (t e))))))

(defun define-menu-internal (&rest items)
  (loop for item in items collect
	(destructuring-bind (name &key submenu application args) item
	  (cons name (if submenu
			 (apply #'define-menu-internal submenu)
			 (apply #'run-application application args))))))

;;;; Public.

(defmacro define-application-menu ((&rest entries))
  "Defines the application menu.
  Entries are of the following form: (entry-name &key callback submenu)

   entry-name (string): 
     string that will be displayed as menu entry.

   :application (string):  
     the name of a program (will be ignore if a submenu is supplied).

   :args (list of string):
     the arguments to pass to the program when called.

   :submenu (list of entries): 
     a list of entries of the form described here. This list of might  be as
     deep as you want to (in the sens of sub-sub-...-menu)."
  `(progn
     (setf eclipse::*menu-1-items*
           ',(apply #'eclipse-extensions::define-menu-internal entries))
     (with-slots ((menu1 eclipse::menu1)) eclipse::*root*
       (setf menu1 (apply #'eclipse::make-pop-up
			  eclipse::*root*
			  eclipse::*menu-1-items*)))))

(defmacro add-application-menu-entry
    ((entry-name &key in-sub-menu-named) &key index application args submenu)
  "Add an entry to the application menu.
   entry-name  (string): 
    a string that will be displayed as menu entry.

   :in-sub-menu-named (string): 
     a string that designate an existing submenu in which the new entry should
     be added.

   :index (or null fixnum): 
     If given the entry will be add at the specified index (from 0). If the
     index is to large then the entry will added at the end of the menu or 
     sub menu if supplied.

   :submenu (list of entries):
     If supplied then the entry that will be added will be a submenu.
     The form that follows :submenu will be expected to be a list of entry:
     (entry-name &key application args submenu) such as described in
     define-application-menu.

   :application (string): 
      the name of a program (will be ignore if a submenu is supplied).

   :args (list of string):
     the arguments to pass to the program when called."
  `(progn
     (setf eclipse::*menu-1-items* 
           (eclipse-extensions::add-entry 
	        ',(car (eclipse-extensions::define-menu-internal 
			   (list entry-name
				 :application application
				 :args args
				 :submenu submenu)))
		,in-sub-menu-named
		,index
		eclipse::*menu-1-items*))
     (with-slots ((menu1 eclipse::menu1)) eclipse::*root*
       (setf menu1 (apply #'eclipse::make-pop-up
			  eclipse::*root*
			  eclipse::*menu-1-items*)))))

(defun remove-application-menu-entry (entry-name &key all-p)
  "Removes, in the application menu, the first or all the entries, if all-p is
  T, that have their name matching the given `entry-name' string."
  (let ((count (if all-p #xFFFF 1)))
    (setf eclipse::*menu-1-items*
	  (remove-entry entry-name eclipse::*menu-1-items* :count count)))
  (with-slots ((menu1 eclipse::menu1)) eclipse::*root*
    (setf menu1 (apply #'eclipse::make-pop-up
		       eclipse::*root*
		       eclipse::*menu-1-items*))))
