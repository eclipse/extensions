;;; -*- Mode: Lisp; Package: ECLIPSE-EXTENSIONS -*-
;;; $Id: commands.lisp,v 1.4 2005/01/16 23:26:02 ihatchondo Exp $
;;;
;;; ECLIPSE. The Common Lisp Window Manager.
;;; Copyright (C) 2004 Iban HATCHONDO
;;; contact : hatchond@yahoo.fr
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program; if not, write to the Free Software
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

;;;
;;; Implementation notes:

;;; This is a collection of the most wanted strokes callback. You'll find the
;;; callbacks in the part named Public. The rest is some macros and functions
;;; that helps in the callbacks definition. At the end of this file you'll
;;; also find some keystroke definition examples. 
;;; The simplest way to use this is to add at the beginning of your eclipse
;;; configuration file a load form like the following:
;;;      (load "<path where you put it>/command.lisp")
;;; or to the compiled file if you prefer. But do not forget to compile it in
;;; environment in which eclipse is defined.
;;; Then define all the strokes you want with as callbacks those functions.

;;; The callbacks are divide in three parts:
;;;  - callbacks that do not apply on a particular widget such as show-desktop.
;;;
;;;  - callbacks that applies on pointered widget. What means that the widget 
;;;    that will be used is the one under the pointer. Usually those callbacks
;;;    will be used by people who use sloppy focus style (:none in Eclipse).
;;;
;;;  - callbacks that applies on focused widget. What means that the widget
;;;    that will be used is the one that is currently focused when keystroke is
;;;    pressed. Usually those callbacks will be used by people who use on click
;;;    focus style (:on-click in Eclipse).

;;; key-combo definitions examples.

;;; (define-key-combo :send-focusded-widget-to-next-workspace
;;;   :keys '(:RIGHT)
;;;   :modifiers '(:and :CONTROL-LEFT :SHIFT-LEFT)
;;;   :default-modifiers-p t
;;;   :fun (action 
;;;          (:press (eclipse-ext:send-focused-widget-to-next-workspace)) ()))

;;; (define-key-combo :send-focused-widget-to-prev-workspace
;;;   :keys '(:LEFT)
;;;   :modifiers '(:and :CONTROL-LEFT :SHIFT-LEFT)
;;;   :default-modifiers-p t
;;;   :fun (action 
;;;          (:press (eclipse-ext:send-focused-widget-to-prev-workspace)) ()))

;;; mouse combo example.

;;; (define-mouse-combo :close-pointered-widget
;;;   ;; pointered is not necessarily the window with the focus in :on-click
;;;   ;; mode. If you want the focused window use close-focused-widget instead.
;;;   ;; It is not kill. E.G: a window may not respond to the message.
;;;   :button 1
;;;   :modifiers '(:and :ALT-LEFT :CONTROL-LEFT)
;;;   :default-modifiers-p t
;;;   :fun (action ()
;;;          (:press
;;;           ;; We can not grab the button release event
;;;           ;; so a mouse action will take place on click.
;;;           (eclipse-ext:close-pointered-widget))))

(in-package :eclipse-extensions)

(export '(show-desktop
	  send-pointered-widget-to-next-workspace
	  send-pointered-widget-to-prev-workspace
	  maximize-pointered-widget iconify-pointered-widget
	  close-pointered-widget kill-pointered-widget
	  send-focused-widget-to-next-workspace
	  send-focused-widget-to-prev-workspace
	  maximize-focused-widget iconify-focused-widget
	  close-focused-widget kill-focused-widget
	  with-pointered-widget with-focused-widget))
	  
;;;; Public macros.

(defmacro with-pointered-widget ((widget) &body forms)
  "Executes the body in a lexical environment where the given symbol
   is binded to the widget located under the mouse pointer."
  (with-gensym (x y ssp child)
    `(multiple-value-bind (,x ,y ,ssp ,child)
         (xlib:query-pointer *root-window*)
       (declare (ignorable ,x ,y ,ssp))
       (let ((,widget (lookup-widget ,child)))
	 ,@forms))))

(defmacro with-focused-widget ((widget) &body forms)
  "Executes the body in a lexical environment where the given symbol
   is binded to the widget that has the focus."
  (let ((window (gensym)))
    `(let* ((,window (netwm:net-active-window *root-window* :window-list t))
	    (,widget (lookup-widget ,window)))
       ,@forms)))

;;;; Private routines.

(defun send-net-wm-desktop-message (widget workspace-dest)
  (event-process 
     (make-event 
         :client-message
	 :type :_net_wm_desktop
	 :data (make-array 1 :element-type '(unsigned-byte 32)
			   :initial-element workspace-dest))
     widget))

(defun migrate-pointered (direction &key (change-workspace t))
  (setf direction (ecase direction (:next #'1+) (:prev #'1-)))
  (with-pointered-widget (widget)
    (let* ((n (number-of-virtual-screens *root-window*))
	   (workspace-dest (mod (funcall direction (current-desk)) n)))
      (typecase widget 
	(decoration (setf widget (get-child widget :application)))
	(application nil)
	(t (return-from migrate-pointered nil)))
      (send-net-wm-desktop-message widget workspace-dest)
      (when change-workspace
	(change-vscreen *root* :n workspace-dest)))))

(defun migrate-focused (direction &key (change-workspace t))
  (setf direction (ecase direction (:next #'1+) (:prev #'1-)))
  (with-focused-widget (widget)
    (let* ((n (number-of-virtual-screens *root-window*))
	   (workspace-dest (mod (funcall direction (current-desk)) n)))
      (typecase widget 
	(decoration (setf widget (get-child widget :application)))
	(application nil)
	(t (return-from migrate-focused nil)))
      (when change-workspace
	(change-vscreen *root* :n workspace-dest)
	(when (eq *focus-type* :on-click) (put-on-top widget)))
      (send-net-wm-desktop-message widget workspace-dest))))

(defun maximize-command (widget &key (direction :both))
  "Maximize a widget in the given direction.
  :DIRECTION (member :both :vertical :horizontal) : indicates the direction
    in which the widget should be maximized."
  (declare (type (member :both :vertical :horizontal) direction))
  (let ((mode (position direction '#(:both :vertical :horizontal))))
    (typecase widget
      (decoration (maximize widget (1+ mode)))
      (application (maximize widget (1+ mode))))))

(defun iconify-command (widget)
  (typecase widget
    (application nil)
    (decoration (setf widget (get-child widget :application)))
    (t (return-from iconify-command nil))))

(defun kill-widget-command (widget)
  (typecase widget 
    (decoration (kill-client-window (get-child widget :application :window t)))
    (application (kill-client-window (widget-window widget)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                                                       ;;;;
;;;;                             Public.                                   ;;;;
;;;;                 Callbacks for "most wanted" strokes.                  ;;;;
;;;;                                                                       ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun show-desktop ()
  "Iconifies all applications on the current virtual screen."
  (mapc (lambda (w) (iconify (lookup-widget w)))
	(screen-content (current-desk) :skip-taskbar nil)))

;; pointered.

(defun send-pointered-widget-to-next-workspace (&key (change-workspace t))
  "Sends the pointered window in the next workspace.
   If :change-workspace is NIL then eclipse will not change the current
   workspace to the one the window just migrate. (default value is T)."
  (migrate-pointered :next :change-workspace change-workspace))

(defun send-pointered-widget-to-prev-workspace (&key (change-workspace t))
  "Sends the pointered window in the previous workspace.
   If :change-workspace is NIL then eclipse will not change the current
   workspace to the one the window just migrate. (default value is T)."
  (migrate-pointered :prev :change-workspace change-workspace))

(defun maximize-pointered-widget (&key (direction :both))
  "Maximize the widget located under the mouse pointer in the given direction.
  :DIRECTION (member :both :vertical :horizontal) : indicates the direction
    in which the widget should be maximized."
  (declare (type (member :both :vertical :horizontal) direction))
  (with-pointered-widget (widget)
    (maximize-command widget :direction direction)))

(defun iconify-pointered-widget ()
  "Iconifies the application located under the mouse pointer."
  (with-focused-widget (widget)
    (iconify-command widget)))

(defun close-pointered-widget ()
  "Closes the application located under the mouse pointer."
  (with-pointered-widget (widget)
    (close-widget widget)))

(defun kill-pointered-widget ()
  "Kills the X resources of the application located under the mouse pointer."
  (with-pointered-widget (widget)
    (kill-widget-command widget)))

;; focused.

(defun send-focused-widget-to-next-workspace (&key (change-workspace t))
  "Sends the focused window in the next workspace.
   If :change-workspace is NIL then eclipse will not change the current
   workspace to the one the window just migrate. (default value is T)."
  (migrate-focused :next :change-workspace change-workspace))

(defun send-focused-widget-to-prev-workspace (&key (change-workspace t))
  "Sends the focused window in the previous workspace.
   If :change-workspace is NIL then eclipse will not change the current
   workspace to the one the window just migrate. (default value is T)."
  (migrate-focused :prev :change-workspace change-workspace))

(defun maximize-focused-widget (&key (direction :both))
  "Maximize the widget that has the focus in the given direction.
  :DIRECTION (member :both :vertical :horizontal) : indicates the direction
    in which the widget should be maximized."
  (declare (type (member :both :vertical :horizontal) direction))
  (with-focused-widget (widget)
    (maximize-command widget :direction direction)))

(defun iconify-focused-widget ()
  "Iconifies the application that has the focus."
  (with-focused-widget (widget)    
    (iconify-command widget)))

(defun close-focused-widget ()
  "Closes the application that has the focus."
  (with-focused-widget (widget)
    (close-widget widget)))

(defun kill-focused-widget ()
  "Kills the X resources of the application that has the focus."
  (with-focused-widget (widget)
    (kill-widget-command widget)))
